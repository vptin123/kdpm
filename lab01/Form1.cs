﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
           
        }
        
        public int DaysInMonth(int month, int year)
        {
            int thang = 0;
            int nam = 0;
            if (txt_Month.Text == "" || txt_Year.Text == "")
            {
                thang = month;
                nam = year;
            }
            else
            {
                 thang = int.Parse(txt_Month.Text);
                 nam = int.Parse(txt_Year.Text);
            }
           
            month = thang;
           
           
            year = nam;
            int daysinmonth = 0;
            switch (thang)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    if (txt_Day.Text == "")
                    {
                        MessageBox.Show("31 Days");
                        daysinmonth = 31;
                    }
                    else
                    {
                        daysinmonth = 31;
                    }
                    break;

                case 4:
                case 6:
                case 9:
                case 11:
                    if (txt_Day.Text == "")
                    {
                        MessageBox.Show("30 Days");
                        daysinmonth = 30;
                    }
                    else
                    {
                        daysinmonth = 30;
                    }
                    break;
                case 2:
                    if (nam % 400 == 0 && txt_Day.Text == "")
                    {
                        MessageBox.Show("29 Days");
                        daysinmonth = 29;
                    }
                    else if (nam % 400 == 0)
                    {
                        daysinmonth = 29;
                    }

                    else 
                    {
                        if (nam % 100 != 0 && txt_Day.Text == "")
                        {
                            MessageBox.Show("28 Days");
                            daysinmonth = 28;
                        }
                        else
                        {
                            daysinmonth = 28;
                            if (nam % 4 == 0 && txt_Day.Text == "")
                            {
                                MessageBox.Show("29 Days");
                                daysinmonth = 29;
                            }
                            else if (nam % 4 == 0)
                            {
                                MessageBox.Show("28 Days");
                                daysinmonth = 28;
                            }
                            else
                            {

                                daysinmonth = 28;
                            }

                        }
                    }
                    break;

            }
            return daysinmonth;
        }
        public bool CheckDays(int day,int month,int year)
        {
            bool flag = false;
            int thang = 0;
            int nam = 0;
            int ngay = 0;
            if (txt_Month.Text == "" || txt_Year.Text == "")
            {
                ngay = day;
                thang = month;
                nam = year;
            }
            else
            {
                ngay = int.Parse(txt_Day.Text);
                thang = int.Parse(txt_Month.Text);
                nam = int.Parse(txt_Year.Text);
            }

            if (thang >= 1 && thang <= 12 && nam >=999 && nam <= 3001)
            {
                if (ngay >= 1)
                {
                    if (ngay <= DaysInMonth(thang, nam))
                    {
                        string mess= string.Format("{0}/{1}/{2} is correct date time !", txt_Day.Text, txt_Month.Text, txt_Year.Text);
                        MessageBox.Show(mess, "Message", 0, MessageBoxIcon.Information);
                        flag = true;
                    }
                    else
                    {
                        string mess = string.Format("{0}/{1}/{2} is NOT correct date time !", txt_Day.Text, txt_Month.Text, txt_Year.Text);
                        MessageBox.Show(mess, "Message", 0, MessageBoxIcon.Information);
                        flag = false;
                    }
                }
                else
                {
                    MessageBox.Show("false");
                }
            }
            else
            {
                MessageBox.Show("false");
            }
            return flag;
        }
        private void btn_Check_Click(object sender, EventArgs e)
        {
            int parsedValue;
            
            //if(txt_Year.Text=="")
            //{
            //    MessageBox.Show("Please input Year");
            //}
            if(txt_Day.Text=="")
            {
                if(txt_Month.Text !="")
                {
                    if (int.TryParse(txt_Month.Text, out parsedValue))
                    {
                        if (int.Parse(txt_Month.Text) >= 1 && int.Parse(txt_Month.Text) <= 12)
                        {
                            if (txt_Year.Text != "")
                            {
                                if(int.TryParse(txt_Year.Text, out parsedValue))
                                {
                                    if (int.Parse(txt_Year.Text) >= 1000 && int.Parse(txt_Year.Text) <= 3000)
                                    {
                                        DaysInMonth(int.Parse(txt_Month.Text), int.Parse(txt_Year.Text));
                                    }
                                    else
                                    {
                                        MessageBox.Show("Input data for Year is out of range","Error",0,MessageBoxIcon.Error);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Input data for Year is incorrect format", "Error", 0, MessageBoxIcon.Error);
                                }
                               
                            }
                            else
                            {
                                MessageBox.Show("Please input Year", "Error", 0, MessageBoxIcon.Error);
                            }

                        }
                        else
                        {
                            MessageBox.Show("Input data for Month is out of range", "Error", 0, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Input data for Month is incorrect format", "Error", 0, MessageBoxIcon.Error);
                    }

                    
                }
                else
                {
                    MessageBox.Show("Please input month", "Error", 0, MessageBoxIcon.Error);
                }
               
            }
            else
            {
                if(txt_Day.Text !="")
                {
                    if(int.TryParse(txt_Day.Text, out parsedValue))
                    {
                        if(int.Parse(txt_Day.Text) >= 1 && int.Parse(txt_Day.Text) <= 31)
                        {
                            if (txt_Month.Text != "")
                            {
                                if (int.TryParse(txt_Month.Text, out parsedValue))
                                {
                                    if (int.Parse(txt_Month.Text) >= 1 && int.Parse(txt_Month.Text) <= 12)
                                    {
                                        if (txt_Year.Text != "")
                                        {
                                            if (int.TryParse(txt_Year.Text, out parsedValue))
                                            {
                                                if (int.Parse(txt_Year.Text) >= 1000 && int.Parse(txt_Year.Text) <= 3000)
                                                {
                                                    CheckDays(int.Parse(txt_Day.Text), int.Parse(txt_Month.Text), int.Parse(txt_Year.Text));
                                                }
                                                else
                                                {
                                                    MessageBox.Show("Input data for Year is out of range", "Error", 0, MessageBoxIcon.Error);
                                                }
                                            }
                                            else
                                            {
                                                MessageBox.Show("Input data for Year is incorrect format", "Error", 0, MessageBoxIcon.Error);
                                            }

                                        }
                                        else
                                        {
                                            MessageBox.Show("Please input Year", "Error", 0, MessageBoxIcon.Error);
                                        }

                                    }
                                    else
                                    {
                                        MessageBox.Show("Input data for Month is out of range", "Error", 0, MessageBoxIcon.Error);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Input data for Month is incorrect format", "Error", 0, MessageBoxIcon.Error);
                                }


                            }
                            else
                            {
                                MessageBox.Show("Please input month", "Error", 0, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Input data for Day is out of range", "Error", 0, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Input data for Day is incorrect format", "Error", 0, MessageBoxIcon.Error);
                        
                    }
                }
            }

        }

        private void btn_Clear_Click(object sender, EventArgs e)
        {
            txt_Day.Text = "";
            txt_Month.Text = "";
            txt_Year.Text = "";
        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (CloseCancel() == false)
            {
                e.Cancel = true;
            };
        }
        public static bool CloseCancel()
        {
            const string message = "Are you sure to exit?";
            const string caption = "Confirm";
            var result = MessageBox.Show(message, caption,
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Information);

            if (result == DialogResult.Yes)
                return true;
            else
                return false;
        }

    }
}
