﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using lab01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace lab01
{
    [TestClass()]
    public class Form1Tests
    {
        [TestMethod()]
        public void Test_DaysInMonth_UTC_01()
        {
            lab01.Form1 a = new lab01.Form1();
           
            
            bool b = false;
            if(a.DaysInMonth(1, 2000)==31)
            {
                b = true;
            }
            Assert.IsTrue(b);
        }
        [TestMethod()]
        public void Test_DaysInMonth_UTC_02()
        {
            lab01.Form1 a = new lab01.Form1();


            bool b = false;
            if (a.DaysInMonth(2, 2000) == 29)
            {
                b = true;
            }
            Assert.IsTrue(b);
        }
        [TestMethod()]
        public void Test_DaysInMonth_UTC_03()
        {
            lab01.Form1 a = new lab01.Form1();


            bool b = false;
            if (a.DaysInMonth(2, 2003) == 28)
            {
                b = true;
            }
            Assert.IsTrue(b);
        }
        [TestMethod()]
        public void Test_DaysInMonth_UTC_04()
        {
            lab01.Form1 a = new lab01.Form1();


            bool b = false;
            if (a.DaysInMonth(3, 2000) == 31)
            {
                b = true;
            }
            Assert.IsTrue(b);
        }
        [TestMethod()]
        public void Test_DaysInMonth_UTC_05()
        {
            lab01.Form1 a = new lab01.Form1();


            bool b = false;
            if (a.DaysInMonth(4, 2000) == 30)
            {
                b = true;
            }
            Assert.IsTrue(b);
        }
        [TestMethod()]
        public void Test_DaysInMonth_UTC_06()
        {
            lab01.Form1 a = new lab01.Form1();


            bool b = false;
            if (a.DaysInMonth(5, 2000) == 31)
            {
                b = true;
            }
            Assert.IsTrue(b);
        }
        [TestMethod()]
        public void Test_DaysInMonth_UTC_07()
        {
            lab01.Form1 a = new lab01.Form1();


            bool b = false;
            if (a.DaysInMonth(6, 2000) == 30)
            {
                b = true;
            }
            Assert.IsTrue(b);
        }
        [TestMethod()]
        public void Test_DaysInMonth_UTC_08()
        {
            lab01.Form1 a = new lab01.Form1();


            bool b = false;
            if (a.DaysInMonth(7, 2000) == 31)
            {
                b = true;
            }
            Assert.IsTrue(b);
        }
        [TestMethod()]
        public void Test_DaysInMonth_UTC_09()
        {
            lab01.Form1 a = new lab01.Form1();


            bool b = false;
            if (a.DaysInMonth(8, 2000) == 31)
            {
                b = true;
            }
            Assert.IsTrue(b);
        }
        [TestMethod()]
        public void Test_DaysInMonth_UTC_10()
        {
            lab01.Form1 a = new lab01.Form1();


            bool b = false;
            if (a.DaysInMonth(9, 2000) == 30 )
            {
                b = true;
            }
            Assert.IsTrue(b);
        }
        [TestMethod()]
        public void Test_DaysInMonth_UTC_11()
        {
            lab01.Form1 a = new lab01.Form1();


            bool b = false;
            if (a.DaysInMonth(10, 2000) == 31)
            {
                b = true;
            }
            Assert.IsTrue(b);
        }
        [TestMethod()]
        public void Test_DaysInMonth_UTC_12()
        {
            lab01.Form1 a = new lab01.Form1();


            bool b = false;
            if (a.DaysInMonth(11, 2000) == 30)
            {
                b = true;
            }
            Assert.IsTrue(b);
        }
        [TestMethod()]
        public void Test_DaysInMonth_UTC_13()
        {
            lab01.Form1 a = new lab01.Form1();


            bool b = false;
            if (a.DaysInMonth(12, 2000) == 31)
            {
                b = true;
            }
            Assert.IsTrue(b);
        }


        [TestMethod()]
        public void Test_CheckDay_UTC_01()
        {
            lab01.Form1 a = new lab01.Form1();
           
       
            var expect = false;
           
            if (a.CheckDays(1, 1, 2006))
            {
                expect = true;
            }
          
            Assert.IsTrue(expect);
        }
        [TestMethod()]
        public void Test_CheckDay_UTC_02()
        {
            lab01.Form1 a = new lab01.Form1();


            var expect = false;

            if (a.CheckDays(2, 2, 2011))
            {
                expect = true;
            }

            Assert.IsTrue(expect);
        }
        [TestMethod()]
        public void Test_CheckDay_UTC_03()
        {
            lab01.Form1 a = new lab01.Form1();


            var expect = false;

            if (a.CheckDays(31, 3, 2006))
            {
                expect = true;
            }

            Assert.IsTrue(expect);
        }
        [TestMethod()]
        public void Test_CheckDay_UTC_04()
        {
            lab01.Form1 a = new lab01.Form1();


            var expect = false;

            if (a.CheckDays(0, 1, 2006))
            {
                expect = false;
            }

            Assert.IsFalse(expect);
        }
        [TestMethod()]
        public void Test_CheckDay_UTC_05()
        {
            lab01.Form1 a = new lab01.Form1();


            var expect = false;

            if (a.CheckDays(32, 1, 2006))
            {
                expect = false;
            }

            Assert.IsFalse(expect);
        }
        public static int ToInt32(string value)
        {
            if (value == null || value == "abc")
                return 0;
            return 1;
        }
        [TestMethod()]
        public void Test_CheckDay_UTC_06()
        {
            lab01.Form1 a = new lab01.Form1();


            var expect = true;
   
            if (a.CheckDays(ToInt32(("abc")), 1, 2006)==false)
            {
                expect = false;
            }

            Assert.IsFalse(expect);
        }
        [TestMethod()]
        public void Test_CheckDay_UTC_07()
        {
            lab01.Form1 a = new lab01.Form1();


            var expect = false;

            if (a.CheckDays(1, 0, 2006))
            {
                expect = false;
            }

            Assert.IsFalse(expect);
        }
        [TestMethod()]
        public void Test_CheckDay_UTC_08()
        {
            lab01.Form1 a = new lab01.Form1();


            var expect = false;

            if (a.CheckDays(1, 13, 2006))
            {
                expect = false;
            }

            Assert.IsFalse(expect);
        }
        [TestMethod()]
        public void Test_CheckDay_UTC_09()
        {
            lab01.Form1 a = new lab01.Form1();


            var expect = true;

            if (a.CheckDays(1, ToInt32(null), 2006)==false)
            {
                expect = false;
            }

            Assert.IsFalse(expect);
        }
        [TestMethod()]
        public void Test_CheckDay_UTC_10()
        {
            lab01.Form1 a = new lab01.Form1();


            var expect = true;

            if (a.CheckDays(1, ToInt32("abc"), 2006) ==false)
            {
                expect = false;
            }

            Assert.IsFalse(expect);
        }
        [TestMethod()]
        public void Test_CheckDay_UTC_11()
        {
            lab01.Form1 a = new lab01.Form1();


            var expect = false;

            if (a.CheckDays(1, 1, 999))
            {
                expect = false;
            }

            Assert.IsFalse(expect);
        }
        [TestMethod()]
        public void Test_CheckDay_UTC_12()
        {
            lab01.Form1 a = new lab01.Form1();


            var expect = false;

            if (a.CheckDays(1, 1, 3001))
            {
                expect = false;
            }

            Assert.IsFalse(expect);
        }
        [TestMethod()]
        public void Test_CheckDay_UTC_13()
        {
            lab01.Form1 a = new lab01.Form1();


            var expect = true;

            if (a.CheckDays(1, 1, ToInt32(null))==false)
            {
                expect = false;
            }

            Assert.IsFalse(expect);
        }
        [TestMethod()]
        public void Test_CheckDay_UTC_14()
        {
            lab01.Form1 a = new lab01.Form1();


            var expect = true;

            if (a.CheckDays(1, 1, ToInt32("abc"))==false)
            {
                expect = false;
            }

            Assert.IsFalse(expect);
        }
        [TestMethod()]
        public void Test_CheckDay_UTC_15()
        {
            lab01.Form1 a = new lab01.Form1();


            var expect = false;

            if (a.CheckDays(31,2,2006))
            {
                expect = false;
            }

            Assert.IsFalse(expect);
        }
    }

}